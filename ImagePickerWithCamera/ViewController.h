//
//  ViewController.h
//  ImagePickerWithCamera
//
//  Created by Rhenz on 2/1/15.
//

#import <UIKit/UIKit.h>




@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)photoLibraryPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;



@end

