//
//  AppDelegate.h
//  ImagePickerWithCamera
//
//  Created by Rhenz on 2/1/15.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

