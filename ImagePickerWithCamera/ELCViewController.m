//
//  ELCViewController.m
//  
//
//  Created by Rhenz on 2/3/15.
//
//

#import "ELCViewController.h"
#import "ELCImagePickerHeader.h"
#import <MobileCoreServices/UTCoreTypes.h>


static NSString *const cellIdentifier = @"collectionCell";

@interface ELCViewController () <ELCImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate>

@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (nonatomic) int totalNumberOfImages;

@end

@implementation ELCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //initialize totalNumberOfImages
    _totalNumberOfImages = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - IBAction
- (IBAction)cameraButtonPressed:(id)sender
{
    if (self.arrayOfImages.count == 10) {
        [self alertWithMessage:@"Maximum image is 10." andTitle:@"Notice"];
    }
    else {
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        cameraPicker.allowsEditing = YES;
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:cameraPicker animated:YES completion:nil];
    }
}

- (IBAction)libraryButtonPressed:(id)sender
{
    
    if (_arrayOfImages.count == 10) {
        [self alertWithMessage:@"Maximum limit of image." andTitle:@"Notice"];
    }
    else {
        ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
        
        elcPicker.maximumImagesCount = 10 - _totalNumberOfImages;
        elcPicker.returnsOriginalImage = YES;
        elcPicker.returnsImage = YES;
        elcPicker.onOrder = YES;
        elcPicker.mediaTypes = @[(NSString *)kUTTypeImage]; // Image only
        elcPicker.imagePickerDelegate = self;
        
        [self presentViewController:elcPicker animated:YES completion:nil];
    }
    
    
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    //    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    NSLog(@"UIImagePickerControllerDelegate");
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    
    if (_arrayOfImages == nil) {
        _arrayOfImages = [NSMutableArray array];
        [_arrayOfImages addObject:image];
    }
    else {
        [_arrayOfImages addObject:image];
    }
    
    
    [self.collectionView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

#pragma mark - ELCImagePickerControllerDelegate
-(void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
//    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    if (!_arrayOfImages) _arrayOfImages = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [_arrayOfImages addObject:image];
            
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                
            }
        }
        
        else {
            NSLog(@"unknown asset type!");
        }
    }
    
//    self.arrayOfImages = images;
    _totalNumberOfImages = _totalNumberOfImages + (int)_arrayOfImages.count;
    
    NSLog(@"arrayOfImages: %@", _arrayOfImages);
    
    //reload table
    [self.collectionView reloadData];
}

-(void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Collection View
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arrayOfImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:200];
    imageView.image = [_arrayOfImages objectAtIndex:indexPath.row];
    
    
    
    return cell;
}

#pragma mark - Methods
- (void)alertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Do nothing
    }];
    
    [alert addAction:cancel];
    
    // Show the alert
    [self presentViewController:alert animated:YES completion:nil];
}


@end
