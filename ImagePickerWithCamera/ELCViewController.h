//
//  ELCViewController.h
//  
//
//  Created by Rhenz on 2/3/15.
//
//

#import <UIKit/UIKit.h>

@interface ELCViewController : UIViewController


- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)libraryButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
