//
//  main.m
//  ImagePickerWithCamera
//
//  Created by Rhenz on 2/1/15.
//  Copyright (c) 2015 Coreproc Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
