//
//  ViewController.m
//  ImagePickerWithCamera
//
//  Created by Rhenz on 2/1/15.
//



// PROBLEM:
// CREATE AN APP WHERE YOU CAN SELECT IMAGES LIMITED TO 10 IMAGES ONLY INCLUDING THE IMAGE TAKEN FROM THE CAMERA.
// DISPLAY THE IMAGES INTO COLLECTION VIEW. UPON DIDSELECT ON COLLECTIONVIEW. AN ACTIONSHEET WILL PROMPT IF YOU WANT TO DELETE/REMOVE IT OR NOT.


/* 
 
 ISSUE:
 
 "connection to assetsd was interrupted or assetsd died" occurs when testing it on a physical device.
*/


#import "ViewController.h"
#import <CTAssetsPickerController.h>


static NSString *const cellIdentifier = @"collectionCell";
static ALAssetsLibrary * assetslibrary;
@interface ViewController () <CTAssetsPickerControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) NSMutableArray *arrayOfImages;
@property (strong, nonatomic) NSMutableArray *arrayOfURLImages;

@property (nonatomic) int totalNumberOfImages;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Set Delegate & Data Source
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    // Initialize total number of images
    _totalNumberOfImages = 0;
    
    assetslibrary = [[ALAssetsLibrary alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Button/IBAction
- (IBAction)cameraButtonPressed:(id)sender
{
    
    if (_totalNumberOfImages == 10) {
        [self alertWithMessage:@"Maximum image is 10." andTitle:@"Notice"];
    }
    else {
        UIImagePickerController *cameraPicker = [[UIImagePickerController alloc] init];
        cameraPicker.delegate = self;
        cameraPicker.allowsEditing = YES;
        cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:cameraPicker animated:YES completion:nil];
    }
    
    
    
}

- (IBAction)photoLibraryPressed:(id)sender
{
    if (_totalNumberOfImages == 10) {
        [self alertWithMessage:@"Maximum images reached. Please remove atleast 1 to add another image." andTitle:@"Notice"];
    }
    else {
        CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
        picker.delegate = self;
        
        // photos only
        picker.assetsFilter = [ALAssetsFilter allPhotos];
        
        // title
        //    picker.title = @"Select up to 10 photos.";
        
        //     selected photos
        //    picker.selectedAssets = [NSMutableArray arrayWithArray:self.arrayOfAssets];
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    
    
}

#pragma mark - UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"UIImagePickerControllerDelegate");
    
//    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
//    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
//    if (_arrayOfImages == nil) {
//        _arrayOfImages = [NSMutableArray array];
//        [_arrayOfImages addObject:image];
//    }
//    else {
//        [_arrayOfImages addObject:image];
//    }
    
//    // URL of Image
//    NSURL *url = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
//    
//    if (!_arrayOfURLImages) _arrayOfURLImages = [[NSMutableArray alloc] init];
//    [_arrayOfURLImages addObject:url];
    
    // Save image to camera roll
    if (!_arrayOfURLImages) _arrayOfURLImages = [[NSMutableArray alloc] init];
    [assetslibrary writeImageToSavedPhotosAlbum:((UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage]).CGImage
                                       metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                completionBlock:^(NSURL *assetURL, NSError *error) {
                                    NSLog(@"assetURL: %@", assetURL);
                                    
                                    
                                    [_arrayOfURLImages addObject:assetURL];
                                    
                                    // Increment Total number of images
                                    _totalNumberOfImages++;
                                    
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        // Reload CollectionView
                                        [self.collectionView reloadData];
                                    });
                                }];
    
    
    // Reload Collection View
//    [self.collectionView reloadData];
    
    // Dismiss
    [self dismissViewControllerAnimated:YES completion:nil];

    
}

#pragma mark - CTAssetPickerControllerDelegate
-(void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    


//    if (!_arrayOfImages) _arrayOfImages = [[NSMutableArray alloc] init];
    // Get the full image in a background thread
    /*dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        for (ALAsset *asset in assets) {
        
            [self.arrayOfImages addObject:asset.defaultRepresentation.url];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"reloading");
            // Reload collectionview
            [self.collectionView reloadData];
            
            [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];

        });
    });*/
    
    
    if (!_arrayOfURLImages) _arrayOfURLImages = [[NSMutableArray alloc] init];
    
    int countOfAddedImage = 0;
    for (ALAsset *asset in assets) {
        [self.arrayOfURLImages addObject:asset.defaultRepresentation.url];
        countOfAddedImage++;
    }
    
    NSLog(@"array of image urls: %@", _arrayOfURLImages);
    
    // Total number of images
    _totalNumberOfImages = _totalNumberOfImages + countOfAddedImage;
    
    
    // Reload Collection View
    [self.collectionView reloadData];
    
    // Dismiss
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
  
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker isDefaultAssetsGroup:(ALAssetsGroup *)group
{
    return ([[group valueForProperty:ALAssetsGroupPropertyType] integerValue] == ALAssetsGroupSavedPhotos);
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset
{
//    if (picker.selectedAssets.count >= 10)
    if (picker.selectedAssets.count >= (10 - _totalNumberOfImages))
//    if (_arrayOfImages.count >= 10)
    {
        int remainingSelection = 10 - _totalNumberOfImages;
        
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Attention"
                                   message:[NSString stringWithFormat:@"Please select not more than %d images.", remainingSelection]
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
    if (!asset.defaultRepresentation)
    {
        UIAlertView *alertView =
        [[UIAlertView alloc] initWithTitle:@"Attention"
                                   message:@"Your asset has not yet been downloaded to your device"
                                  delegate:nil
                         cancelButtonTitle:nil
                         otherButtonTitles:@"OK", nil];
        
        [alertView show];
    }
    
//    return (picker.selectedAssets.count < 10 && asset.defaultRepresentation != nil);
    return (picker.selectedAssets.count < (10 - _totalNumberOfImages) && asset.defaultRepresentation != nil);
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldEnableAsset:(ALAsset *)asset
{
    // Video not allowed
    if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo])
    {
        //        NSTimeInterval duration = [[asset valueForProperty:ALAssetPropertyDuration] doubleValue];
        //        return lround(duration) >= 5;
        return NO;
    }
    // Photos are always enabled
    else
    {
        return YES;
    }
}

#pragma mark - CollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}





-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    return self.arrayOfImages.count;
//    return self.arrayOfURLImages.count;
    return _totalNumberOfImages;
}





-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSURL *imageURL = [_arrayOfURLImages objectAtIndex:indexPath.row];
    
    [assetslibrary assetForURL:imageURL resultBlock:^(ALAsset *asset) {
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
//        UIImage *image = [UIImage imageWithCGImage:representation.fullResolutionImage scale:0.25f orientation:(UIImageOrientation)[representation orientation]];
        UIImage *image = [UIImage imageWithCGImage:asset.thumbnail];
        imageView.image = image;
        
    } failureBlock:^(NSError *error) {
        // Do nothing.
        NSLog(@"error!!");
    }];
    
    
    return cell;
}

#pragma mark - CollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Do you want to delete this image?" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *delete = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        // Delete image url
        [_arrayOfURLImages removeObjectAtIndex:indexPath.row];
        
        // Decrement total number of images
        _totalNumberOfImages--;
        
        // Reload collection view
        [self.collectionView reloadData];
        
        NSLog(@"count of arrayURLImages: %d", _arrayOfURLImages.count);
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Do nothing
    }];
    
    [actionSheet addAction:delete];
    [actionSheet addAction:cancel];
    
    
    // Present Alert Controller
    [self presentViewController:actionSheet animated:YES completion:nil];
}




#pragma mark - Methods
- (void)alertWithMessage:(NSString *)message andTitle:(NSString *)title
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Do nothing
    }];
    
    [alert addAction:cancel];
    
    // Show the alert
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)writeAsset:(ALAsset *)asset toPath:(NSString *)path
{
    ALAssetRepresentation *representation = asset.defaultRepresentation;
    long long size = representation.size;
    NSMutableData *rawData = [[NSMutableData alloc] initWithCapacity:size];
    void *buffer = [rawData mutableBytes];
    [representation getBytes:buffer fromOffset:0 length:size error:nil];
    NSData *assetData = [[NSData alloc] initWithBytes:buffer length:size];
    [assetData writeToFile:path atomically:YES];
}

@end
